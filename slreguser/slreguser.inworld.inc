<?php
/**
 * @package	SlUser
 * @subpackage SlRegUser
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlUser is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Route commands
*/
function slreguser_inworld_controller($params) {
  $response = array();
  $cmd = $params['cmd'];
  $sl = &$params['sl'];
  $args = $params['args'];

  switch ($cmd) {
    case 'checkRegistration':
      $response = slreguser_check_registration($sl, $args);
      break;

    case 'fullRegister':
      $response = slreguser_full_register($sl, $args);
      break;

    case 'quickRegister':
      $response = slreguser_quick_register($sl, $args);
      break;

    default:
      $response['status']   = FALSE;
      $response['message']  = t('Oops! Unknown command:'). $cmd;
      break;
  }
  $sl->response['status']   = $response['status'];
  $sl->response['message']  = $response['message'];
}

/**
* Check user registration
*/
function slreguser_check_registration($sl, $args) {
  $response = array();

  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  // check if slreguser parameters allow registration
  if (variable_get('slreguser_global_settings_full_registration_enable', 1) == 0) {
    $response['status']   = FALSE;
    $response['message']  = 'registration closed;'. t('Registration is closed.');
    return $response;
  }

  // check if the user already have a sluser account
  $values = array(
    'uuid' =>$args['user_key']
  );
  $sluser = sluser_load($values);
  if ($sluser->id) {
    $account = user_load($sluser->uid);
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('You already have an account named "!name" on the site.', array('!name' => $account->name));
    return $response;
  }

  // check if the user name is already registered on the site
  $account = user_load(array('name' =>$args['website_username']));
  if ($account->uid != 0) {
    $response['status']   = FALSE;
    $response['message']  = 'already registered;'. t('User with this name is already registered.');
    return $response;
  }

  // user is not registered so return success
  $response['status']   = TRUE;
  $response['message']  = 'success;'. t('Start registering process.');
  return $response;
}

/**
* Register the user
*/
function slreguser_full_register($sl, $args) {
  $reponse = array();
  $msg = '';

  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  // check if the user already have a sluser account
  $values = array(
    'uuid' =>$args['user_key']
  );
  $sluser = sluser_load($values);
  if ($sluser->id) {
    $account = user_load($sluser->uid);
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('You already have an account named "!name" on the site.', array('!name' => $account->name));
    return $response;
  }

  // Verify the syntax of the given name.
  $check_username = user_validate_name($args['website_username']);
  if ($check_username != "") {
    $response['status']   = FALSE;
    $response['message']  = "error;". $check_username;
    return $response;
  }

  // verify the email
  $check_email = user_validate_mail($args['email']);
  if ($check_email != "") {
    $response['status']   = FALSE;
    $response['message']  = "error;". $check_email;
    return $response;
  }

  $name   = $args['website_username'];
  $mail   = $args['email'];
  $pass   = $args['pass'];
  $status = 1;

/*
 * SEP: changed - block user *only* if administrator approval required.
 */
  // define if the user needs to validate email
// TODO : check this
  if (variable_get('slreguser_global_settings_email_verification', 0)) {
    $status = 0;
  }

  // register the user
  $values = array(
    'name'    =>$name,
    'mail'    =>$mail,
    'pass'    =>$pass,
    'status'  =>$status,
  );
  // check for username replacement
  if (variable_get('sluser_global_settings_profile_force_username', 0) && ($args['sl_username'] != $sluser->account->name)) {
    $values['name'] = $args['sl_username'];
    $msg = t("Your username on the web site does not match your avatar name. Your username has been changed and is now the same as your avatar's name. Please use this name to log into the web site.");
  }
  // change the role for the user
  $values['roles'] = $sluser->account->roles + array_filter(variable_get('sluser_global_settings_roles', array()));
  if (variable_get('sluser_global_settings_roles_replace', 0)) {
    $values['roles'] = array_filter(variable_get('sluser_global_settings_roles', array()));
  }
  $account = user_save('', $values);
  if (!$account) {
    $response['status'] = FALSE;
    $response['message'] = "error;". t("Error saving user account.");
    return $response;
  }

  // add or update the sluser
  $args['uid']  = $account->uid;
  $args['name'] = $args['sl_username'];
  $args['uuid'] = $args['user_key'];
  unset($args['sl_username']);
  $saved = sluser_save($args);
  $response['status']   = $saved['status'];
  $response['message']  = $saved['message'];
  if ($saved['status'] == FALSE) {
    return $response;
  }

  watchdog('user', 'SlRegUser inworld : New user: %name (%email).', array('%name' => $name, '%email' => $mail), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

  // Add plain text password into user account to generate mail tokens.
    $account->password = $pass;

  // user is registered so check if the user needs a confirmation email
  if (variable_get('slreguser_global_settings_email_verification', 0)) {
    // Create new user account, no administrator approval required but need to confirm email.
    _user_mail_notify('register_no_approval_required', $account);
    $response['status']   = TRUE;
    $response['message']  = "success need activate;". t('Your password and further instructions have been sent to your e-mail address.'). ' '. $msg;
    return $response;
  }
  else if (variable_get('slreguser_global_settings_admin_approval_required', 0)) {
    // Create new user account, administrator approval required.
    _user_mail_notify('register_pending_approval', $account);
    $response['status']   = TRUE;
    $response['message']  = "success need activate;". t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your e-mail address.'). ' '. $msg;
    return $response;
  }
  else if ($status == 1) {
    // Create new user account, no administrator approval required.
    _user_mail_notify('register_no_approval_required', $account);
    $response['status']   = TRUE;
    $response['message']  = "success reg complete;". t('Your password and further instructions have been sent to your e-mail address.'). ' '. $msg;
    return $response;
  }
}


/**
* Register the user
*/
function slreguser_quick_register($sl, $args) {
  global $base_url;
  $reponse = array();

  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  // check if quick registration is allowed
  if (isset($args['quick_register']) && $args['quick_register'] == 1) {
    if (variable_get('slreguser_global_settings_quick_registration_enable', 1) == 0) {
      $response['status']   = FALSE;
      $response['message']  = 'registration closed;'. t('Registration is closed.');
      return $response;
    }
  }

  // Verify the syntax of the given name.
  $args['website_username'] = sluser_fullname_to_username($args['website_username']);
  $check_username = user_validate_name($args['website_username']);
  if ($check_username != "") {
    $response['status']   = FALSE;
    $response['message']  = "error;". $check_username;
    return $response;
  }

  $values = new stdclass;

  // check sluser
  $sluser = sluser_load(array('uuid' => $args['user_key']));
  if ($sluser->id) {
    // if an account already exists but is not the inworld username, create another one
    //$registered = user_load(array('uid' => ($sluser->uid)));
    //if ($registered->name == $args['website_username']) {
      $values->uid = $sluser->uid;
    //}
  }
  else {
    // build the sluser (needed to create email address)
    $sluser = new stdclass;
    $sluser->name = $args['website_username'];
    $sluser->uuid = $args['user_key'];
    $sluser->account = new stdClass;
    $sluser->account->roles = array();
  }

  // register the user
  $values->name    = $args['website_username'];
  $values->mail    = slreguser_build_email_address($sluser);
  $values->pass    = user_password(6);
  $values->status  = 1;
  // change the role for the user
  $values->roles = $sluser->account->roles + array_filter(variable_get('sluser_global_settings_roles', array()));
  if (variable_get('sluser_global_settings_roles_replace', 0)) {
    $values->roles = array_filter(variable_get('sluser_global_settings_roles', array()));
  }

  if ($values->uid) {
    $account = user_save($values, (array)$values);
  }
  else {
    $account = user_save('', (array)$values);
  }

  if (!$account) {
    $response['status'] = FALSE;
    $response['message'] = "error;". t("Error saving user account.");
    return $response;
  }

  // add or update the sluser
  $args['uid']  = $account->uid;
  $args['name'] = $args['sl_username'];
  $args['uuid'] = $args['user_key'];
  unset($args['sl_username']);
  $saved = sluser_save($args);
  $response['status']   = $saved['status'];
  $response['message']  = $saved['message'];
  if ($saved['status'] == FALSE) {
    return $response;
  }

  watchdog('user', 'SlRegUser inworld quick register : New user: %name (%email).', array('%name' => $name, '%email' => $mail), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

  $response['status']   = FALSE;
  $response['message']  = "success reg complete;". t('Your account is enabled. You can log in the website : !link using your inworld username : "!username" and your password is : "!pass". (without quotes and case sensitive)', array('!link'=>$base_url. '/user', '!username'=>$account->name, '!pass'=>$values->pass));
  return $response;
}
