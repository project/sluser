<?php
/**
 * @package	SlUser
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlUser is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Route commands
*/
function sluser_inworld_controller($params) {
  $response = array();
  $cmd = $params['cmd'];
  $sl = &$params['sl'];
  $args = $params['args'];

  switch ($cmd) {
    case 'registerTerminal':
    case 'updateTerminal':
      $response = sluser_register_terminal($sl, $args);
      break;

    case 'checkRegKey':
      $response = sluser_check_reg_key($sl, $args);
      break;

    case 'log':
      $response = sluser_log($sl, $args);
      break;

    default:
      $response['status']   = FALSE;
      $response['message']  = t('Oops! Unknown command:'). $cmd;
      break;
  }
  $sl->response['status']   = $response['status'];
  $sl->response['message']  = $response['message'];
}

/**
* Register the terminal
*/
function sluser_register_terminal($sl, $args) {
  $response = array();

  // check the terminal password
  if (!sluser_terminal_check_password($args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  $values = array(
    'sim_hostname'=>$args['sim_hostname'],
    'ownerkey'    =>$sl->ownerkey,
    'ownername'   =>$sl->ownername,
    'name'        =>$sl->objectname,
    'uuid'        =>$sl->objectkey,
    'region'      =>$sl->region_name,
    'position'    =>strtr($sl->position, array('(' =>'', ')' =>'', ',' =>'/', ' '=>'')),
    'rpc_channel' =>$args['rpc_channel']
  );
  if ($args['http_url'] != '') {
    $values['http_url'] = $args['http_url'];
  }
  $registered = sluser_terminal_save($values);
  $response['status']   = $registered['status'];
  $response['message']  = $registered['message'];
  return $response;
}

/**
* Check the user's reg key
*/
function sluser_check_reg_key($sl, $args) {
  $response = array();
  $msg = '';

  // check the terminal password
  if (!sluser_terminal_check_password($args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  $user_key = $args['user_key'];
  $user_name = sluser_fullname_to_username($args['user_name']);

  // check if the user is already registered
  $values = array(
    'name' =>$user_name
  );
  $sluser = sluser_load($values);
  if ($sluser->uid) {
    $account = user_load($sluser->uid);
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('You already have an account named !name registered with this avatar.', array('!name' =>$account->name)).';'.$user_key;
    return $response;
  }

  // get the user
  $values = array(
    'reg_key' =>$args['reg_key']
  );
  $sluser = sluser_load($values);
  if (!$sluser->id) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Could not find user.').';'.$user_key;
    return $response;
  }

  // add values to the user
  $values = array(
    'uid'     => $sluser->uid,
    'name'    => $user_name,
    'uuid'    => $user_key,
    'reg_key' =>$args['reg_key']
  );
  $registered = sluser_save($values);

  $values = array();

  // change the role for the user
  $values['roles'] = $sluser->account->roles + array_filter(variable_get('sluser_global_settings_roles', array()));
  if (variable_get('sluser_global_settings_roles_replace', 0)) {
    $values['roles'] = array_filter(variable_get('sluser_global_settings_roles', array()));
  }

  // check for username replacement
  $replace_name = variable_get('sluser_global_settings_profile_force_username', 0) && ($user_name != $sluser->account->name);
  if ($replace_name) {
    // rename the user
    $values['name'] = $user_name;
    $msg = t("Your username on the web site does not match your avatar name. Your username has been changed and is now the same as your avatar's name. Please use this name to log into the web site.");
  }

  $account = new stdclass;
  $account->uid = $sluser->uid;
  $account = user_save($account, $values);
  if (!$account) {
    $response['status']   = FALSE;
    $response['message']  = "error;". t("Error saving user account.").';'. $user_key;
    return $response;
  }

  // refresh pathauto
  if (module_exists('pathauto') && $replace_name) {
    pathauto_user_update_alias($account, 'update');
  }

  $response['status']   = $registered['status'];
  $response['message']  = $registered['message']. ' '. $msg. ';'. $user_key;
  return $response;
}

function sluser_log($sl, $args) {
  $response = array();
  $msg = '';

  // check the terminal password
  if (!sluser_terminal_check_password($args['password'], $args['keypass'])) {
    $response['status']   = FALSE;
    $response['message']  = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  watchdog('SlUser', '!message', array('!message' => $args['message']), (int)$args['severity']);
}
