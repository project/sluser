<?php
?>
<div class="picture">
  <img width="<?php print $sluser->profile_picture_size_w; ?>px" height="<?php print $sluser->profile_picture_size_h; ?>px" alt="<?php print t('[no picture available]'); ?>" title="<?php print $sluser->name; ?>" src="<?php print $sluser->profile_picture_link; ?>"/>
</div>