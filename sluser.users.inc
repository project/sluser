<?php
/**
 * @package	SlUser
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlUser is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
 * Implementation of hook_user().
 */
function sluser_user($op, &$edit, &$account, $category = NULL) {
  $admin = user_access('administer users');

  // get default values for the displayed items
  $items = sluser_global_settings_profile_build_items_list();
  $default_items = variable_get('sluser_user_settings_privacy_default_view_items', $items['default']);

  switch ($op) {

    case 'validate':
      switch ($category) {
        case variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser'):
          // validate the uploaded picture
          if ($edit['sluser_user_settings_profile_picture_type'] == 1) {
            sluser_user_settings_picture_validate_picture($edit);
          }
          if (isset($edit['sluser_user_settings_profile_picture_type']) && $edit['sluser_user_settings_profile_picture_type'] == 3) {
            if (isset($edit['sluser_user_settings_profile_picture_external_picture_path'])) {
              if (!valid_url($edit['sluser_user_settings_profile_picture_external_picture_path'], TRUE)) {
                form_set_error('sluser_user_settings_profile_picture_external_picture_path', t('Wrong external picture URL.'));
                drupal_add_js(drupal_get_path('module', 'sluser') .'/sluser.js');
              }
            }
          }
          return array(
          // tab view params
            'sluser_user_settings_profile_privacy_tab_view_enable' => isset($edit['sluser_user_settings_profile_privacy_tab_view_enable']) ? $edit['sluser_user_settings_profile_privacy_tab_view_enable'] : variable_get('sluser_global_settings_profile_privacy_tab_view_default_enable', 1),
            'sluser_user_settings_profile_privacy_tab_view_items' => isset($edit['sluser_user_settings_profile_privacy_tab_view_items']) ? $edit['sluser_user_settings_profile_privacy_tab_view_items'] : variable_get('sluser_global_settings_profile_privacy_tab_view_default_items', $default_items),
          // profile view params
            'sluser_user_settings_profile_privacy_profile_view_enable' => isset($edit['sluser_user_settings_profile_privacy_profile_view_enable']) ? $edit['sluser_user_settings_profile_privacy_profile_view_enable'] : variable_get('sluser_global_settings_profile_privacy_profile_view_default_enable', 1),
            'sluser_user_settings_profile_privacy_profile_view_items' => isset($edit['sluser_user_settings_profile_privacy_profile_view_items']) ? $edit['sluser_user_settings_profile_privacy_profile_view_items'] : variable_get('sluser_global_settings_profile_privacy_profile_view_default_items', $default_items),
          // TODO : validate external link
          );
          break;
      }
      break;

    case 'insert':
      // generate a reg key
      sluser_generate_regkey($account);
      // set default params
      return array(
      // tab view params
      $edit['sluser_user_settings_profile_privacy_tab_view_enable'] = variable_get('sluser_global_settings_profile_privacy_tab_view_default_enable', 1),
      $edit['sluser_user_settings_profile_privacy_tab_view_items'] = variable_get('sluser_global_settings_profile_privacy_tab_view_default_items', $default_items),
      // profile view params
      $edit['sluser_user_settings_profile_privacy_profile_view_enable'] = variable_get('sluser_global_settings_profile_privacy_profile_view_default_enable', 1),
      $edit['sluser_user_settings_profile_privacy_profile_view_items'] = variable_get('sluser_global_settings_profile_privacy_profile_view_default_items', $default_items),
      );
      break;

    case 'delete':
      // get the sluser
      $sluser = sluser_load(array('uid' =>$account->uid));
      if (!empty($sluser->id)) {
        sluser_delete($sluser->id);
        drupal_set_message(t('SlUser deleted.'));
      }
      break;

    case 'view':
      if (sluser_view_user_profile_access($account)) {
        sluser_view_user_profile($account);
      }
      break;

    case 'form':
      switch ($category) {
        case variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser'):
          return sluser_user_settings_form($account, $edit);
          break;
      }
      break;

    case 'categories':
      return array(
      array(
            'name'    => variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser'),
            'title'   => variable_get('sluser_global_settings_terminology_sluser_human_name',  'SlUser'),
            'weight'  => 1,
      ),
      );
      break;
  }
}

/**
 * 
 */
function sluser_view_user_profile_access($account) {
  return $account->sluser_user_settings_profile_privacy_profile_view_enable && variable_get('sluser_global_settings_profile_privacy_profile_view_enable', 1);
}

/**
 * View user form
 */
function sluser_view_user_profile($account) {
  // get the sluser
  $sluser = sluser_load(array('uid' =>$account->uid));

  // display the label
  if (($sluser->id && $sluser->name != '' && $sluser->uuid != '') || (user_edit_access($account))) {
    $account->content['sluser'] = array(
      '#type'   => 'user_profile_category',
      '#title'  => variable_get('sluser_global_settings_terminology_sluser_human_name',  'SlUser'),
    );
  }

  // reg key
  if (user_edit_access($account)) {
    $account->content['sluser']['regkey'] = array(
      '#type'   => 'user_profile_item',
      '#title'  => t('Reg Key'),
      '#value'  => $sluser->reg_key,
      '#weight' => 1,
    );
  }

  // check if user is registered inworld
  if (empty($sluser->uuid)) {
    drupal_set_message(t('User not registered InWorld.'));
  }

  // get the display items status
  $items = $account->sluser_user_settings_profile_privacy_profile_view_items;

  // picture
  if ($items['picture'] && variable_get('sluser_global_settings_profile_picture_enabled', 0)) {
    $sluser->profile_picture_link = sluser_build_profile_picture_link($sluser);
    if ($items['picture']) {
      $account->content['sluser']['picture'] = array(
        '#type'   => 'user_profile_item',
        '#value'  => theme('sluser_picture', $sluser),
        '#weight' => 0,
      );
    }
  }

  // name
  if ($items['avatar_name'] && $sluser->name != '') {
    $account->content['sluser']['name'] = array(
      '#type'   => 'user_profile_item',
      '#title'  => t('Name'),
      '#value'  => $sluser->name,
      '#weight' => 0,
    );
  }

  // uuid
  if ($items['avatar_uuid'] && $sluser->uuid != '') {
    $account->content['sluser']['uuid'] = array(
      '#type'   => 'user_profile_item',
      '#title'  => t('Uuid'),
      '#value'  => $sluser->uuid,
      '#weight' => 0,
    );
  }

  // external profile link
  if ($items['external_profile_link'] && $sluser->uuid != '') {
    $account->content['sluser']['web_profile_link'] = array(
      '#type'   => 'user_profile_item',
      '#title'  => t('Web profile'),
      '#value'  => '<a href="'. sluser_build_external_profile_link($sluser). '" title="'. t('Click here to see the web profile'). '">'. t('Click here to see the web profile'). '</a>',
      '#weight' => 11,
    );
  }

  // inworld profile link
  if ($items['inworld_profile_link'] && $sluser->uuid != '') {
    $account->content['sluser']['inworld_profile_link'] = array(
      '#type'   => 'user_profile_item',
      '#title'  => t('Inworld profile'),
      '#value'  => '<a href="'. sluser_build_inworld_profile_link($sluser). '" title="'. t('Click here to see the inworld profile'). '">'. t('Click here to see the inworld profile'). '</a>',
      '#weight' => 12,
    );
  }
}

/**
 *
 */
function sluser_user_settings_form($form_state, &$edit) {
  $form = array();
  // privacy
  if (variable_get('sluser_global_settings_profile_privacy_tab_view_enable', 1) || variable_get('sluser_global_settings_profile_privacy_profile_view_enable', 1)) {
    $form['sluser']['user']['settings'][] = sluser_user_settings_privacy_form($form_state, $edit);
  }
  // picture
  if (variable_get('sluser_global_settings_profile_picture_enabled', 0)) {
    $form['sluser']['user']['settings'][] = sluser_user_settings_picture_form($form_state, $edit);
  }
  $form['sluser']['user']['settings'][] = sluser_user_settings_regkey_form($form_state, $edit);
  return $form;
}

/**
 *
 */
function sluser_user_settings_privacy_form($form_state, &$edit) {
  $form = array();

  // fieldset for privacy
  $form['privacy'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Privacy'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // build the items lists
  $items = sluser_global_settings_profile_build_items_list();

  if (variable_get('sluser_global_settings_profile_privacy_tab_view_enable', 1)) {
    // tab view
    $form['privacy']['tab_view'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Tab view'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
    );
    // allow users to see the sluser tab
    $form['privacy']['tab_view']['sluser_user_settings_profile_privacy_tab_view_enable'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Display tab view'),
      '#default_value'  => isset($edit['sluser_user_settings_profile_privacy_tab_view_enable']) ? $edit['sluser_user_settings_profile_privacy_tab_view_enable'] : variable_get('sluser_global_settings_profile_privacy_tab_view_default_enable', 1),
      '#description'    => t('Allow other users to see the profile tab.'),
    );
    // choose what items to display in tab view
    $form['privacy']['tab_view']['sluser_user_settings_profile_privacy_tab_view_items'] = array(
      '#type'           => 'checkboxes',
      '#title'          => t('Items to show'),
      '#default_value'  => isset($edit['sluser_user_settings_profile_privacy_tab_view_items']) ? $edit['sluser_user_settings_profile_privacy_tab_view_items'] : variable_get('sluser_global_settings_profile_privacy_tab_view_default_items', $items['default']),
      '#options'        => $items['values'],
      '#description'    => t("Define the items to show on the profile tab."),
    );
  }

  if (variable_get('sluser_global_settings_profile_privacy_profile_view_enable', 1)) {
    // profile view
    $form['privacy']['default_view'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Profile view'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
    );
    // allow users to see the sluser tab
    $form['privacy']['default_view']['sluser_user_settings_profile_privacy_profile_view_enable'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Display profile view'),
      '#default_value'  => isset($edit['sluser_user_settings_profile_privacy_profile_view_enable']) ? $edit['sluser_user_settings_profile_privacy_profile_view_enable'] : variable_get('sluser_global_settings_profile_privacy_profile_view_default_enable', 1),
      '#description'    => t('Allow other users to see the profile tab.'),
    );

    // choose what items to display in tab view
    $form['privacy']['default_view']['sluser_user_settings_profile_privacy_profile_view_items'] = array(
      '#type'           => 'checkboxes',
      '#title'          => t('Items to show'),
      '#default_value'  => isset($edit['sluser_user_settings_profile_privacy_profile_view_items']) ? $edit['sluser_user_settings_profile_privacy_profile_view_items'] : variable_get('sluser_global_settings_profile_privacy_profile_view_default_items', $items['default']),
      '#options'        => $items['values'],
      '#description'    => t("Define the items to show on the profile tab."),
    );
  }
  return $form;
}

/**
 *
 */
function sluser_user_settings_regkey_form($form_state, &$edit) {
  $form = array();

  // fieldset for regkey
  $form['reg_key'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Regkey'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );
  // reg key renew
  $form['reg_key']['renew_regkey_container'] = array(
    '#value'  => '&nbsp;',
    '#prefix' => '<div id="renew-regkey-container">',
    '#suffix' => '</div>',
  );
  $form['reg_key']['item'] = array(
    '#type'   => 'item',
    '#title'  => t('Regkey'),
    '#value'  => t("Push the button if you would like to renew your regkey."),
  );
  $machine_name = variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser');
  $form['reg_key']['renew_regkey_button'] = array(
    '#type'   => 'submit',
    '#value'  => t('Renew Regkey'),
    '#weight' => 1,
    '#ahah'   => array(
      'path'    => $machine_name. '/users/renew-regkey/js',
      'wrapper' => 'renew-regkey-container',
      'method'  => 'replace',
      'effect'  => 'fade',
  ),
  );

  return $form;
}

/**
 *
 */
function sluser_user_settings_picture_form($form_state, &$edit) {
  global $base_url;
  $form = array();

  // fieldset for profile picture
  $form['picture'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Profile picture'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  // build the list choice
  $options = array(
  t('Disabled'),
  t('Upload new picture'),
  t('Get from inworld profile'),
  t('Get from an external link')
  );
  if ($edit['picture'] != '') {
    $options[] = t('Same as main profile');
  }

  // get the picture type
  $form['picture']['sluser_user_settings_profile_picture_type'] = array(
    '#type'           => 'radios',
    '#title'          => t('Picture support'),
    '#default_value'  => isset($edit['sluser_user_settings_profile_picture_type']) ? $edit['sluser_user_settings_profile_picture_type'] : 0,
    '#options'        => $options,
    '#prefix'         => '<div class="sluser-user-settings-profile-picture-type-radios">',
    '#suffix'         => '</div>',
  );

  drupal_add_js(drupal_get_path('module', 'sluser') .'/sluser.js');

  $picture_support = $edit['sluser_user_settings_profile_picture_type'];

  // upload
  $css_class = 'sluser-user-settings-profile-picture-type-upload';
  if ($picture_support != 1) {
    $css_class .= ' js-hide';
  }
  $form['picture']['upload'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Profile picture'),
    '#collapsible'  => FALSE,
    '#collapsed'    => FALSE,
    '#prefix' => '<div class="'. $css_class .'">',
    '#suffix' => '</div>',
  );
  if (!empty($edit['sluser_user_settings_profile_picture_path'])) {
    $form['picture']['upload']['sluser_user_settings_profile_picture_current_picture'] = array('#value' => '<div class="picture"><img src="'. $base_url. '/'. $edit['sluser_user_settings_profile_picture_path']. '"/></div>');
    $form['picture']['upload']['sluser_user_settings_profile_picture_picture_delete'] = array('#type' => 'checkbox', '#title' => t('Delete picture'), '#description' => t('Check this box to delete your current picture.'));
  }
  else {
    $form['picture']['upload']['sluser_user_settings_profile_picture_picture_delete'] = array('#type' => 'hidden');
  }
  $form['picture']['upload']['#attributes'] = array('enctype' => "multipart/form-data");
  $form['picture']['upload']['sluser_user_settings_profile_picture_upload'] = array('#type' => 'file', '#title' => t('Upload picture'), '#size' => 48, '#description' => t('Your virtual face or picture. Maximum dimensions are %dimensions and the maximum size is %size kB.', array('%dimensions' => variable_get('sluser_global_settings_profile_picture_internal_picture_dimensions', '85x85'), '%size' => variable_get('sluser_global_settings_profile_picture_internal_picture_file_size', '30'))) .' '. variable_get('sluser_global_settings_profile_picture_internal_picture_guidelines', ''));

  // external
  $css_class = 'sluser-user-settings-profile-picture-type-external';
  if ($picture_support != 3) {
    $css_class .= ' js-hide';
  }
  $form['picture']['external'] = array(
    '#prefix' => '<div class="'. $css_class .'">',
    '#suffix' => '</div>',
  );
  $form['picture']['external']['sluser_user_settings_profile_picture_external_picture_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('External image path'),
    '#default_value'  => isset($edit['sluser_user_settings_profile_picture_external_picture_path']) ? $edit ['sluser_user_settings_profile_picture_external_picture_path'] : '',
    '#size'           => 30,
    '#maxlength'      => 255,
    '#description'    => t('Provide a path to get the picture.'),
  );

  return $form;
}

function sluser_user_settings_picture_validate_picture(&$form_state) {

  $account = & $form_state['_account'];
  if ($form_state['sluser_user_settings_profile_picture_picture_delete']) {
    if (isset($account->sluser_user_settings_profile_picture_path) && file_exists($account->sluser_user_settings_profile_picture_path)) {
      file_delete($account->sluser_user_settings_profile_picture_path);
    }
    $form_state['sluser_user_settings_profile_picture_path'] = "";
  }
  // If required, validate the uploaded picture.
  $validators = array(
    'file_validate_is_image'          => array(),
    'file_validate_image_resolution'  => array(variable_get('sluser_global_settings_profile_picture_internal_picture_dimensions', '85x85')),
    'file_validate_size'              => array(variable_get('sluser_global_settings_profile_picture_internal_picture_file_size', '30') * 1024),
  );
  if ($file = file_save_upload('sluser_user_settings_profile_picture_upload', $validators)) {
    // Remove the old picture.
    if (isset($account->sluser_user_settings_profile_picture_path) && file_exists($account->sluser_user_settings_profile_picture_path)) {
      file_delete($account->sluser_user_settings_profile_picture_path);
    }

    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    $info = image_get_info($file->filepath);
    $destination = variable_get('sluser_global_settings_profile_picture_internal_picture_path', 'pictures') .'/'. variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser'). '-picture-'. $account->uid .'.'. $info['extension'];
    if (file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
      $form_state['sluser_user_settings_profile_picture_path'] = $file->filepath;
    }
    else {
      form_set_error('sluser_user_settings_profile_picture_upload', t("Failed to upload the picture image; the %directory directory doesn't exist or is not writable.", array('%directory' => variable_get('sluser_global_settings_profile_picture_internal_picture_path', 'pictures'))));
    }
  }
}

/**
 * Generates a regkey
 */
function sluser_generate_regkey($account) {
  // generate a reg key
  $reg_key = module_invoke('user', 'password');
  $values = array(
        'uid' => $account->uid,
        'reg_key' => $reg_key
  );
  //drupal_write_record('sluser', $values);
  sluser_save($values);
  // don't show message if user is not enabled
  if ($account->status) {
    drupal_set_message(t('SlUser updated : reg key is : '). $reg_key);
  }
}

/**
 * AHAH callback to renew the regkey
 */
function sluser_renew_regkey_js() {
  // drupal's alchemy
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  $renew_regkey_form = $form['sluser']['renew_regkey_container'];
  unset($renew_regkey_form['#prefix'], $renew_regkey_form['#suffix']);
  // generate the regkey
  sluser_generate_regkey($form['_account']['#value']);
  // return the value
  $output = theme('status_messages') . drupal_render($renew_regkey_form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Determine if a user can access to the contact tab.
 */
function sluser_view_user_tab_access($account) {
  return $account->sluser_user_settings_profile_privacy_tab_view_enable && variable_get('sluser_global_settings_profile_privacy_tab_view_enable', 1);
}

/**
 * Personal contact page.
 */
function sluser_view_user_tab($account) {
  global $user;

  // check if the user has a sluser
  $sluser = sluser_load(array('uid' =>$account->uid));
  if (empty($sluser->uuid)) {
    drupal_set_message(t('User not registered InWorld.'));
  }

  // build some usefull sentences
  $machine_name = variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser');
  $sluser->reg_key_description = t('Use this key to register inworld with a terminal. Select a terminal on !url.', array('!url' => l(t('the terminals list page'), $machine_name. '/terminals')));

  // define if the user can view some private infos
  $sluser->canedit = user_edit_access($account);

  // render the theme
  $output = theme('sluser_view_user_tab_display', $sluser);

  // ask for others panels from others modules
  $panels = module_invoke_all('sluser_view_user_tab', $account);
  foreach ($panels as $panel) {
    $output .= $panel;
  }
  return $output;
}

/**
 * Preprocess the template for the tab view
 */
function template_preprocess_sluser_view_user_tab_display(&$variables) {
  $variables['sluser'] = $variables[0];
  // get the display items status
  $variables['sluser']->items = $variables['sluser']->account->sluser_user_settings_profile_privacy_tab_view_items;

  // name
  if (!$variables['sluser']->items['avatar_name'] || $variables['sluser']->name == '') {
    unset($variables['sluser']->name);
  }
  // uuid
  if (!$variables['sluser']->items['avatar_uuid'] || $variables['sluser']->uuid == '') {
    unset($variables['sluser']->uuid);
  }
  // external profile link
  if ($variables['sluser']->items['external_profile_link'] && $variables['sluser']->uuid != '') {
    $variables['sluser']->external_profile_link = sluser_build_external_profile_link($variables['sluser']);
  }
  // inworld profile link
  if ($variables['sluser']->items['inworld_profile_link'] && $variables['sluser']->uuid != '') {
    $variables['sluser']->inworld_profile_link = sluser_build_inworld_profile_link($variables['sluser']);
  }
  // profile picture
  if ($variables['sluser']->items['picture'] && variable_get('sluser_global_settings_profile_picture_enabled', 0)) {
    $variables['sluser']->picture = theme('sluser_picture', $variables['sluser']);
  }
}

/**
 * Preprocess the template for the picture
 */
function template_preprocess_sluser_picture(&$variables) {
  $variables['sluser']        = $variables[0];
  $variables['sluser']->items = $variables['sluser']->account->sluser_user_settings_profile_privacy_tab_view_items;

  if ($variables['sluser']->items['picture'] && variable_get('sluser_global_settings_profile_picture_enabled', 0)) {
    $variables['sluser']->profile_picture_link = sluser_build_profile_picture_link($variables['sluser']);
    list($width, $height) = explode('x', variable_get('sluser_global_settings_profile_picture_internal_picture_dimensions', '85x85'));
    $variables['sluser']->profile_picture_size_w = $width;
    $variables['sluser']->profile_picture_size_h = $height;
  }
}

/**
 * Load the data for a single sluser
 */
function sluser_load($array) {
  // Dynamically compose a SQL query:
  $query = array();
  $params = array();

  if (is_numeric($array)) {
    $array = array('uid' => $array);
  }
  elseif (!is_array($array)) {
    return FALSE;
  }

  foreach ($array as $key => $value) {
    if ($key == 'uid' || $key == 'id') {
      $query[] = "$key = %d";
      $params[] = $value;
    }
    else {
      $query[]= "LOWER($key) = LOWER('%s')";
      $params[] = $value;
    }
  }
  $query = db_query('SELECT * FROM {sluser} u WHERE '. implode(' AND ', $query), $params);

  if (!$sluser = db_fetch_object($query)) {
    $sluser = FALSE;
  }

  // get the linked user
  $account = user_load(array('uid' =>$sluser->uid));
  $account = drupal_unpack($account, 'data');
  $sluser->account = $account;

  return $sluser;
}

/**
 * Store the sluser
 */
function sluser_save($values) {

  // convert values to an object
  $values = (object)$values;

  $response = array();
  // check if the sluser already exists
  $sluser = new stdclass;
  $data = array();
  if ($values->id != 0) {
    $data['id'] = $values->id;
  }
  if ($values->uid != 0) {
    $data['uid'] = $values->uid;
  }
  $sluser = sluser_load($data);

  if (!$sluser->id) {
    // insert new entry
    $response['op'] = drupal_write_record('sluser', $values);
  }
  else {
    // update entry
    $values->id = $sluser->id;
    $response['op'] = drupal_write_record('sluser', $values, 'id');
  }

  // define the result status
  $response['status'] = TRUE;
  switch ($response['op']) {
    case SAVED_NEW:
      $response['message'] = 'success;'. t('New user added.')."/".print_r($values,true);
      break;
    case SAVED_UPDATED:
      $response['message'] = 'success;'. t('User updated.');
      break;
    default:
      $response['status'] = FALSE;
      $response['message'] = 'error;'. t('Error saving user.').$response['op'];
      break;
  }
  $response['id'] = $values->id;
  return $response;
}

/**
 * Delete the sluser.
 */
function sluser_delete($sluser_id) {
  // get the sluser
  $sluser = sluser_load(array('id' =>$sluser_id));

  // remove sluser
  db_query('DELETE FROM {sluser} WHERE id = %d', $sluser_id);

  return $sluser;
}

/**
 * Build the external profile link
 */
function sluser_build_external_profile_link($sluser) {
  $link = variable_get('sluser_global_settings_profile_external_profile_link', 'http://world.secondlife.com/resident/%uuid');
  // define the path replacers
  $replacers = array('%uuid' =>$sluser->uuid, '%name' =>$sluser->name);
  // replace the strings
  foreach ($replacers as $key=>$value) {
    $link = str_replace($key, $value, $link);
  }
  return $link;
}

/**
 * Build the inworld profile link
 */
function sluser_build_inworld_profile_link($sluser) {
  $link = variable_get('sluser_global_settings_profile_inworld_profile_link', 'secondlife:///app/agent/%uuid/about');
  // define the path replacers
  $replacers = array('%uuid' =>$sluser->uuid, '%name' =>$sluser->name);
  // replace the strings
  foreach ($replacers as $key=>$value) {
    $link = str_replace($key, $value, $link);
  }
  return $link;
}

/**
 * Build the profile image link
 */
function sluser_build_profile_picture_link($sluser) {
  $image_link = '';
  switch ($sluser->account->sluser_user_settings_profile_picture_type) {

    case 1: // upload
      if ($sluser->account->sluser_user_settings_profile_picture_path != '' && file_exists($sluser->account->sluser_user_settings_profile_picture_path)) {
        $image_link = file_create_url($sluser->account->sluser_user_settings_profile_picture_path);
      }
      break;

    case 2: // inworld
      // get the external profile type
      $profile_type = variable_get('sluser_global_settings_profile_picture_external_profile_type', 0);
      switch ($profile_type) {
        case 0: // secondlife profile
          // get the image uuid
          $profile_image = secondlife_extract_picture_key(secondlife_get_linden_user_profile($sluser->uuid));
          // return the link
          if ($profile_image != '') {
            $image_link = "http://secondlife.com/app/image/". $profile_image. "/1";
          }
          break;
        case 1: // custom
          $link = variable_get('sluser_global_settings_profile_picture_external_profile_link', 'http://secondlife.com/services/user/small_image/%uuid.jpg');
          if ($link != '') {
            // define the path replacers
            $replacers = array('%uuid' =>$sluser->uuid, '%name' =>$sluser->name);
            // replace the strings
            foreach ($replacers as $key=>$value) {
              $link = str_replace($key, $value, $link);
            }
            $image_link = $link;
          }
          break;
      }
      break;

    case 3: // external
      $image_link = $sluser->account->sluser_user_settings_profile_picture_external_picture_path;
      break;

    case 4: // same
      if ($sluser->account->picture != '') {
        $image_link = file_create_url($sluser->account->picture);
      }
      break;
  }

  // replace image with the default link if no picture found
  if (empty($image_link) && variable_get('sluser_global_settings_profile_picture_internal_picture_default','')) {
    return variable_get('sluser_global_settings_profile_picture_internal_picture_default','');
  }
  return $image_link;
}

/*
 * Convert full name to username
 */
function sluser_fullname_to_username($name) {
 if (variable_get('sluser_global_settings_profile_use_username', 1)) {
   $array_name = explode(' ', $name);
   if ($array_name[1] == 'Resident') {
     return strtolower($array_name[0]);
   }
   return strtolower($array_name[0]. '.'. $array_name[1]);
 }
 return $name;
}

/*
 * Convert username to full name
 */
function sluser_username_to_fullname($name) {
  $array_name = explode('.', $name);
  if (!isset($array_name[1]) || empty($array_name[1])) {
   return $array_name[0]. ' Resident';
  }
  return $array_name[0]. ' '. $array_name[1];
}
