Drupal.behaviors.userSettings = function (context) {
  // global settings
  $('div.sluser-global-settings-profile-picture-enabled input[type=radio]:not(.userSettings-processed)', context).addClass('userSettings-processed').click(function () {
	switch(this.value) {
      case "0": // disabled
        $('div.sluser-global-settings-profile-picture-internal', context).hide();
        $('div.sluser-global-settings-profile-picture-external', context).hide();
        break;
      case "1": // internal
        $('div.sluser-global-settings-profile-picture-internal', context).show();
        $('div.sluser-global-settings-profile-picture-external', context).hide();
        break;
      case "2": // external
        $('div.sluser-global-settings-profile-picture-internal', context).hide();
        $('div.sluser-global-settings-profile-picture-external', context).show();
        break;
      case "3": // both
	    $('div.sluser-global-settings-profile-picture-internal', context).show();
	    $('div.sluser-global-settings-profile-picture-external', context).show();
	    break;
    }
  });
  // global settings sluser_global_settings_profile_picture_external_direct_link_type
  $('div.sluser-global-settings-profile-picture-external-profile-type input[type=radio]:not(.userSettings-processed)', context).addClass('userSettings-processed').click(function () {
		switch(this.value) {
	      case '0': // secondlife
	        $('div.sluser-global-settings-profile-picture-external-profile-link', context).hide();
	        break;
	      case '1': // custom
	        $('div.sluser-global-settings-profile-picture-external-profile-link', context).show();
	        break;
	    }
	  });
  // user settings
  $('div.sluser-user-settings-profile-picture-type-radios input[type=radio]:not(.userSettings-processed)', context).addClass('userSettings-processed').click(function () {
	switch(this.value) {
      case '0': // disabled
      case '2': // inworld
      case '4': // same
        $('div.sluser-user-settings-profile-picture-type-upload', context).hide();
        $('div.sluser-user-settings-profile-picture-type-external', context).hide();
        break;
      case '1': // upload
        $('div.sluser-user-settings-profile-picture-type-upload', context).show();
        $('div.sluser-user-settings-profile-picture-type-external', context).hide();
        break;
      case '3': // external
    	$('div.sluser-user-settings-profile-picture-type-upload', context).hide();
    	$('div.sluser-user-settings-profile-picture-type-external', context).show();
	    break;
    }
  });
};