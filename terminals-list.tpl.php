<?php
?>
<?php if (count($terminals) > 0): ?>
<div id="sluser-terminals">
  <?php foreach ($terminals as $terminal): ?>
  <div class="sluser-terminal-container profile" style="border:1px solid black;padding:10px;margin:20px;">
    <dl>
      <dt><?php print t('Sim hostname'); ?></dt>
        <dd><?php print $terminal->sim_hostname; ?></dd>
      <dt><?php print t('Status'); ?></dt>
        <dd><span id="terminal-<?php print $terminal->id; ?>-status"><span><?php print t('Checking....'); ?></span></span></dd>
      <dt><?php print t('Name'); ?></dt>
        <dd><?php print $terminal->name; ?></dd>
      <dt><?php print t('Region'); ?></dt>
        <dd><?php print $terminal->region; ?></dd>
      <dt><?php print t('Position'); ?></dt>
        <dd><?php print $terminal->position; ?></dd>
      <?php $slurl = 'http://slurl.com/secondlife/'.$terminal->region.'/'.$terminal->position.'/?&title='.$terminal->name.'&msg='.t('Here is a terminal.'); ?>
      <dt><a href="<?php echo $slurl; ?>" target="_blank"><?php print t('Teleport to the terminal'); ?></a></dt>
      <?php if ($allow_edit): ?>
      <dt><a href="terminals/<?php print $terminal->id; ?>/delete"><?php print t("Delete the terminal"); ?></a></dt>
      <?php endif; ?>
    </dl>
  </div>
  <?php endforeach; ?>
  <?php print $pager; ?>
</div>
<?php else: ?>
  <?php print t('No terminals registered.'); ?>
<?php endif; ?>
