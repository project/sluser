<?php
?>
<div class="profile">

  <h3><?php print t('Information'); ?></h3>

  <?php if (isset($sluser->profile_picture_link)): ?>
  <?php print $sluser->picture;?>
  <?php endif; ?>
  <dl>
    <?php if (isset($sluser->name)): ?>
    <dt><?php print t('Avatar name'); ?></dt>
      <dd><?php print $sluser->name; ?></dd>
    <?php endif; ?>
    <?php if (isset($sluser->uuid)): ?>
    <dt><?php print t('Avatar UUID'); ?></dt>
      <dd><?php print $sluser->uuid; ?></dd>
    <?php endif; ?>
    <?php if (isset($sluser->external_profile_link)): ?>
    <dt><?php print t('Web profile'); ?></dt>
      <dd><a href="<?php print $sluser->external_profile_link; ?>" title="<?php print t('Click here to see the web profile'); ?>"><?php print t('Click here to see the web profile'); ?></a></dd>
    <?php endif; ?>
    <?php if (isset($sluser->inworld_profile_link)): ?>
    <dt><?php print t('Inworld profile'); ?></dt>
      <dd><a href="<?php print $sluser->inworld_profile_link; ?>" title="<?php print t('Click here to see the inworld profile'); ?>"><?php print t('Click here to see the inworld profile'); ?></a></dd>
    <?php endif; ?>

    <?php if ($sluser->canedit): ?>
    <?php if ($sluser->reg_key != ""): ?>
    <dt><?php print t('Registration key'); ?></dt>
      <dd><?php print $sluser->reg_key; ?>
        <div class="description"><?php print $sluser->reg_key_description; ?></div>
      </dd>
    <?php endif; ?>
    <?php endif; ?>
  </dl>
</div>