<?php
/**
 * @package	SlUser
 * @subpackage SlPoints
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlUser is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Route commands
*/
function slpoints_inworld_controller($params) {
  $response = array();
  $cmd = $params['cmd'];
  $sl = &$params['sl'];
  $args = $params['args'];

  switch ($cmd) {
    case 'getPoints':
      $response = slpoints_get_points($sl, $args);
      break;

    case 'setPoints':
      $response = slpoints_set_points($sl, $args);
      break;

    case 'withdraw':
      $response = slpoints_withdraw($sl, $args);
      break;
    default:
      $response['status'] = FALSE;
      $response['message'] = t('Oops! Unknown command:'). $cmd;
      break;
  }
  $sl->response['status'] = $response['status'];
  $sl->response['message'] = $response['message'];
}

/**
* Register the terminal
*/
function slpoints_get_points($sl, $args) {
  $response = array();

  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Wrong terminal password.');
    return $response;
  }

  $user_key = $args['user_key'];
  $user_name = $args['user_name'];

  // get the user
  $values = array(
    'uuid' =>$user_key
  );
  $sluser = sluser_load($values);
  if (!$sluser->id) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Could not find user.'). ';'. $user_key;
    return $response;
  }

  // get points
  $points_credit = userpoints_get_current_points($sluser->uid);

  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('You have now : '). $points_credit. ' '. variable_get('userpoints_trans_lcpoints', 'points'). ';'. $user_key;
  return $response;
}

/**
* Check the user's reg key
*/
function slpoints_set_points($sl, $args) {
  $response = array();

  $user_key     = $args['user_key'];
  $user_name = $args['user_name'];
  $amount       = $args['amount'];

  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Wrong terminal password.'). ';'. $user_key. ';'. $amount;
    return $response;
  }

  // get the user
  $values = array(
    'uuid' =>$user_key
  );
  $sluser = sluser_load($values);
  if (!$sluser->id) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Could not find user.'). ';'. $user_key. ';'. $amount;
    return $response;
  }

  $slpoints_relation = variable_get('sluser_slpoints_relation', 1);
  $points = $amount / $slpoints_relation;
  if ($points < 1) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Minimum amount is : '). $slpoints_relation. ';'. $user_key. ';'. $amount;
    return $response;
  }

  // set points
  $params = array(
    'uid' => $sluser->uid,
    'operation' => 'insert',
    'points' => $points,
  );
  userpoints_userpointsapi($params);

  // get points
  $points_credit = userpoints_get_current_points($sluser->uid);

  watchdog('SlUser', 'SlPoints : User: %name added %points to his account, giving %L$.', array('%name' => $sluser->name, '%points' => $points, '%L$' => $amount), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('You have now : '). $points_credit. ' '. variable_get('userpoints_trans_lcpoints', 'points'). ';'. $user_key;
  return $response;
}

function slpoints_withdraw($sl, $args) {
  // check the terminal password
  if (!module_invoke('sluser', 'terminal_check_password', $args['password'], $args['keypass'])) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Wrong terminal password.');
    return $response;
  }
  // get the sluser
  $sluser = sluser_load(array('uuid' => $args['user_key']));
  $msg = t('Confirmed withdraw to !name : amount = !amount ; TID = !tid', array('!name' => $args['user_name'], '!amount' => $args['amount'], '!tid' => $args['tid']));
  watchdog('SlUser', '!message', array('!message' => $msg), 4);
}
