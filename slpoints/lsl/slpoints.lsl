// $Id: slpoints.lsl,v 1.4 2009/10/05 19:21:42 ssm2017binder Exp $
// @version SlUser
// @package SlPoints
// @copyright Copyright wene / ssm2017 Binder (C) 2009. All rights reserved.
// @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
// SlUser is free software and parts of it may contain or be derived from the GNU General Public License
// or other free or open source software licenses.

// **********************
//          STRINGS
// **********************
// symbols
string _SYMBOL_RIGHT = "✔";
string _SYMBOL_WRONG = "✖";
string _SYMBOL_WARNING = "⚠";
string _SYMBOL_RESTART = "↺";
string _SYMBOL_HOR_BAR_1 = "⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌";
string _SYMBOL_HOR_BAR_2 = "⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊";
string _SYMBOL_ARROW = "⤷";
// === common ===
string _RESET = "Reset";
string _CLOSE = "Close";
string _MENU_TIMEOUT = "Menu time-out. Please try again.";
string _ADD_MONEY = "Add money";
string _GET_AMOUNT = "Get amount";
string _GETTING_POINTS = "Getting points";
string _SETTING_POINTS = "Setting points";
string _REFUNDING = "Refunding";
string _SENDING_WITHDRAW = "Sending withdraw";
string _ONLY_ONE_USER_PER_REQUEST_PLEASE = "Only one user per request please";
// === params ===
string _PARSE_PARAMS_ERROR = "Parse params error in about module";
// http errors
string _REQUEST_TIMED_OUT = "Request timed out";
string _FORBIDDEN_ACCESS = "Forbidden access";
string _PAGE_NOT_FOUND = "Page not found";
string _INTERNET_EXPLODED = "the internet exploded!!";
string _SERVER_ERROR = "Server error";
string _METHOD_NOT_SUPPORTED = "Method unsupported";
// ===================================================
//          NOTHING SHOULD BE CHANGED UNDER THIS LINE
// ===================================================
// **********************
//          VARS
// **********************
string url = "";
string url2 = "";
string password = "";
integer display_info = 1;
integer update_speed = 30;
integer busy_time = 30;
key owner;
integer busy = FALSE;
key actual_user = NULL_KEY;
integer listener;
// separators
string HTTP_SEPARATOR = ";";
string PARAM_SEPARATOR = "||";
// **********************
//          CONSTANTS
// **********************
integer RESET = 20000;
integer ADD_APP = 20001;
integer ENABLE_COUNTDOWN = 20002;
integer LOG_MESSAGE = 70015;
// status
integer SET_READY = 20011;
integer SET_ENABLED = 20012;
integer SET_DISABLED = 20013;
integer SET_BUSY = 20014;
// params
integer SET_PARAMS = 20025;
integer GET_PARAMS = 20026;
// user
integer SET_ACTUAL_USER = 20101;
integer GET_ACTUAL_USER = 20102;
// money
integer GIVE_MONEY = 70081;
// **********************
//          FUNCTIONS
// **********************
integer parseParams(string params) {
    // url | url2 | display_info | update_speed | busy_time | http_separator
    list values = llParseStringKeepNulls(params, [PARAM_SEPARATOR], []);
    url = llList2String(values, 0);
    url2 = llList2String(values, 1);
    password = llList2String(values, 2);
    display_info = llList2Integer(values, 3);
    update_speed = llList2Integer(values, 4);
    busy_time = llList2Integer(values, 5);
    HTTP_SEPARATOR = llList2String(values, 6);
    if (url != "" && url2 != "") {
        return TRUE;
    }
    return FALSE;
}
// **********************
//          HTTP
// **********************
// get points
key getPointsId;
getPoints(key userKey) {
    if ( display_info ) {
        llOwnerSay(_GETTING_POINTS);
    }
    // build password
    integer keypass = (integer)llFrand(9999)+1;
    string md5pass = llMD5String(password, keypass);
    // send the request
    getPointsId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        // common values
                        "app=slpoints"
                        +"&cmd=getPoints"
                        +"&output_type=message"
                        +"&arg="
                        // password
                        +"password="+md5pass+":"
                        +"keypass="+(string)keypass+":"
                        // user values
                        +"user_key="+(string)userKey+":"
                        +"user_name="+llKey2Name(userKey));
}
// withdraw
key withdrawId;
withdraw(key userKey, string username, integer amount, string tid) {
    if ( display_info ) {
        llOwnerSay(_SENDING_WITHDRAW);
    }
    // build password
    integer keypass = (integer)llFrand(9999)+1;
    string md5pass = llMD5String(password, keypass);
    // send the request
    withdrawId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        // common values
                        "app=slpoints"
                        +"&cmd=withdraw"
                        +"&output_type=message"
                        +"&arg="
                        // password
                        +"password="+md5pass+":"
                        +"keypass="+(string)keypass+":"
                        // user values
                        +"user_key="+(string)userKey+":"
                        +"user_name="+username+":"
                        +"amount="+(string)amount+":"
                        +"tid="+tid);
}
// set points
key setPointsId;
integer sent_amount = 0;
key sent_userKey = NULL_KEY;
setPoints(integer amount, key userKey) {
    if ( display_info ) {
        llOwnerSay(_SETTING_POINTS);
    }
    sent_amount = amount;
    sent_userKey = userKey;
    // build password
    integer keypass = (integer)llFrand(9999)+1;
    string md5pass = llMD5String(password, keypass);
    // send the request
    setPointsId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        // common values
                        "app=slpoints"
                        +"&cmd=setPoints"
                        +"&output_type=message"
                        +"&arg="
                        // password
                        +"password="+md5pass+":"
                        +"keypass="+(string)keypass+":"
                        // user values
                        +"amount="+(string)amount+":"
                        +"user_key="+(string)userKey+":"
                        +"user_name="+llKey2Name(userKey));
}
// get server answer
getServerAnswer(integer status, string body) {
    if (status == 499) {
        llOwnerSay(_SYMBOL_WARNING+ " "+ (string)status+ " "+ _REQUEST_TIMED_OUT);
    }
    else if (status == 403) {
        llOwnerSay(_SYMBOL_WARNING+ " "+ (string)status+ " "+ _FORBIDDEN_ACCESS);
    }
    else if (status == 404) {
        llOwnerSay(_SYMBOL_WARNING+ " "+ (string)status+ " "+ _PAGE_NOT_FOUND);
    }
    else if (status == 500) {
        llOwnerSay(_SYMBOL_WARNING+ " "+ (string)status+ " "+ _SERVER_ERROR);
    }
    else if (status != 403 && status != 404 && status != 500) {
        llOwnerSay(_SYMBOL_WARNING+ " "+ (string)status+ " "+ _INTERNET_EXPLODED);
        llOwnerSay(body);
    }
}
list parsePostData(string message) {
    list postData = [];         // The list with the data that was passed in.
    list parsedMessage = llParseString2List(message,["&"],[]);    // The key/value pairs parsed into one list.
    integer len = ~llGetListLength(parsedMessage);
 
    while(++len) {          
        string currentField = llList2String(parsedMessage, len); // Current key/value pair as a string.
 
        integer split = llSubStringIndex(currentField,"=");     // Find the "=" sign
        if(split == -1) { // There is only one field in this part of the message.
            postData += [llUnescapeURL(currentField),""];  
        } else {
            postData += [llUnescapeURL(llDeleteSubString(currentField,split,-1)), llUnescapeURL(llDeleteSubString(currentField,0,split))];
        }
    }
    // Return the strided list.
    return postData ;
}
giveMoney(string str) {
    list incomingMessage = parsePostData(str); // 1 = password / 3 = key / 5 = uuid / 7 = name / 9 = amount / 11 = tid
    key userkey = llList2Key(incomingMessage, 5);
    string username = llList2String(incomingMessage, 7);
    integer amount = llList2Integer(incomingMessage, 9);
    string tid = llList2String(incomingMessage, 11);
    llGiveMoney(userkey, amount);
    string msg = _SENDING_WITHDRAW+ " "+ username+ " L$ "+ (string)amount+ " TID "+ tid;
    withdraw(userkey, username, amount, tid);
    llInstantMessage(owner, msg);
    //llMessageLinked(LINK_THIS, LOG_MESSAGE, "4"+ PARAM_SEPARATOR+ msg, NULL_KEY);
}
// ********** DIALOG FUNCTIONS **********
// Dialog constants
integer lnkDialog = 14001;
integer lnkDialogNotify = 14004;
integer lnkDialogResponse = 14002;
integer lnkDialogTimeOut = 14003;
integer lnkDialogReshow = 14011;
integer lnkDialogCancel = 14012;
string seperator = "||";
integer dialogTimeOut = 0;
string packDialogMessage(string message, list buttons, list returns){
    string packed_message = message + seperator + (string)dialogTimeOut;
    integer i;
    integer count = llGetListLength(buttons);
    for(i=0; i<count; i++){
        string button = llList2String(buttons, i);
        if(llStringLength(button) > 24) button = llGetSubString(button, 0, 23);
        packed_message += seperator + button + seperator + llList2String(returns, i);
    }
    return packed_message;
}
dialogReshow(){llMessageLinked(LINK_THIS, lnkDialogReshow, "", NULL_KEY);}
dialogCancel(){
    llMessageLinked(LINK_THIS, lnkDialogCancel, "", NULL_KEY);
    llSleep(1);
}
dialog(key id, string message, list buttons, list returns){
    llMessageLinked(LINK_THIS, lnkDialog, packDialogMessage(message, buttons, returns), id);
}
dialogNotify(key id, string message){
    list rows;
    llMessageLinked(LINK_THIS, lnkDialogNotify,
        message + seperator + (string)dialogTimeOut + seperator,
        id);
}
// ********** END DIALOG FUNCTIONS **********
// **********************
//          MAIN ENTRY
// **********************
default {
    state_entry() {
        owner = llGetOwner();
    }
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == SET_READY) {
            state getParams;
        }
        else if (num == RESET) {
            llResetScript();
        }
    }
}
// **********************
//          GET PARAMS
// **********************
state getParams {
    state_entry() {
        llMessageLinked(LINK_THIS, ADD_APP, _ADD_MONEY, NULL_KEY);
        llMessageLinked(LINK_THIS, ADD_APP, _GET_AMOUNT, NULL_KEY);
        llMessageLinked(LINK_THIS, GET_PARAMS, "", NULL_KEY);
    }
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == SET_PARAMS) {
            if (parseParams(str)) {
                state getPerms;
            }
            else {
                llOwnerSay(_PARSE_PARAMS_ERROR);
            }
        }
        else if (num == RESET) {
            llResetScript();
        }
    }
}
// **********************
//          GET PERMS
// **********************
state getPerms {
    state_entry() {
        llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
    }
    run_time_permissions(integer perms) {
        if (perms & PERMISSION_DEBIT) {
            state wait;
        }
    }
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == RESET) {
            llResetScript();
        }
    }
}
// ************************
//            WAIT MODE
// ************************
state wait {
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == SET_BUSY) {
            busy = TRUE;
            actual_user = id;
        }
        else if (num == SET_ENABLED) {
            busy = FALSE;
            actual_user = NULL_KEY;
        }
        else if (num == SET_ACTUAL_USER) {
            actual_user = id;
        }
        else if (num == lnkDialogTimeOut) {
            dialogNotify(id, _MENU_TIMEOUT);
            state wait;
        }
        else if (num == lnkDialogResponse) {
            if (str == _RESET) {
                llResetScript();
            }
            else if (str == _ADD_MONEY) {
                llMessageLinked(LINK_THIS, SET_BUSY, "", actual_user);
                state addMoney;
            }
            else if (str == _GET_AMOUNT) {
                llMessageLinked(LINK_THIS, SET_BUSY, "", actual_user);
                state getAmount;
            }
        }
        else if (num == GIVE_MONEY) {
          giveMoney(str);
        }
        else if (num == RESET) {
            llResetScript();
        }
    }
}
// ************************
//            ADD MONEY
// ************************
state addMoney {
    money(key giver, integer amount) {
        if (giver == actual_user) {
            setPoints(amount, giver);
        }
        else {
            llInstantMessage(giver, _ONLY_ONE_USER_PER_REQUEST_PLEASE);
            llGiveMoney(giver, amount);
            llInstantMessage(giver, _REFUNDING);
        }
    }
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == SET_ENABLED) {
            busy = FALSE;
            actual_user = NULL_KEY;
            state wait;
        }
        else if (num == RESET) {
            llResetScript();
        }
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id != getPointsId && request_id != setPointsId) {
            return;
        }
        if (status != 200) {
            getServerAnswer(status, body);
            if (sent_amount > 0 && sent_userKey != NULL_KEY) {
                llGiveMoney(sent_userKey, sent_amount);
                llInstantMessage(sent_userKey, _REFUNDING);
            }
            sent_amount = 0;
            sent_userKey = NULL_KEY;
        }
        else {
            // get the values
            body = llStringTrim( body , STRING_TRIM);
            list values = llParseStringKeepNulls(body,[HTTP_SEPARATOR],[]);
            string answer = llList2String(values, 0);
            string message = llList2String(values, 1);
            string userKey = llList2String(values, 2);
            integer amount = llList2Integer(values, 3);
            if ( answer == "success") {
                llInstantMessage((key)userKey, message);
            }
            else if ( answer == "error" ) {
                llInstantMessage((key)userKey, message);
                llGiveMoney(userKey, amount);
                llInstantMessage((key)userKey, _REFUNDING);
            }
            sent_amount = 0;
            sent_userKey = NULL_KEY;
            llMessageLinked(LINK_THIS, SET_ENABLED, "", actual_user);
            state wait;
        }
    }
}
// ************************
//            GET AMOUNT
// ************************
state getAmount {
    state_entry() {
        getPoints(actual_user);
    }
    link_message(integer sender_num, integer num, string str, key id) {
        if (num == SET_ENABLED) {
            busy = FALSE;
            actual_user = NULL_KEY;
            state wait;
        }
        else if (num == RESET) {
            llResetScript();
        }
    }
    http_response(key request_id, integer status, list metadata, string body) {
        if (request_id != getPointsId && request_id != setPointsId) {
            return;
        }
        if (status != 200) {
            getServerAnswer(status, body);
        }
        else {
            // get the values
            body = llStringTrim( body , STRING_TRIM);
            list values = llParseStringKeepNulls(body,[HTTP_SEPARATOR],[]);
            string answer = llList2String(values, 0);
            string message = llList2String(values, 1);
            string userKey = llList2String(values, 2);
            integer amount = llList2Integer(values, 3);
            if ( answer == "success") {
                llInstantMessage((key)userKey, message);
            }
            else if ( answer == "error" ) {
                llInstantMessage((key)userKey, message);
            }
            llMessageLinked(LINK_THIS, SET_ENABLED, "", actual_user);
            state wait;
        }
    }
}