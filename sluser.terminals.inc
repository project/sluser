<?php
/**
 * @package	SlUser
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlUser is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

DEFINE('GET_STATUS', 70010);
DEFINE('GIVE_MONEY', 70081);

/**
 * Terminals list page
 */
function sluser_terminals_list() {
  $terminals = sluser_terminal_load_all();
  $output = theme('sluser_terminals_list', $terminals);
  return $output;
}

/**
* Check the terminal status
*/
function sluser_check_terminal_status_js() {
  $terminal_id = $_GET['id'];
  $status = "offline";

  // get the terminal
  $terminal = sluser_terminal_load($terminal_id);

  $http_answer = new stdclass;
  if ($terminal->http_url != '') {
    // request status using http_request
    $http_answer = drupal_http_request(base64_decode($terminal->http_url). '/get_status/', array(), 'GET');
  }
  else {
    $http_answer->code = 404;
  }
  
  if ($http_answer->code == 200) {
    $status = $http_answer->data;
  }
  else {
    // request status using xmlrpc
    $answer = secondlife_rpc ($terminal->rpc_channel, GET_STATUS, "check status");
    $status = $answer['string'];
  }

  switch ($status) {
    case 'online':
      print '<span style="font-weight:bold;color:green">'. t('Online'). '</span>';
      break;

    case 'disabled':
      print '<span style="font-weight:bold;color:orange">'. t('Disabled'). '</span>';
      break;

    case 'offline':
    default:
      print '<span style="font-weight:bold;color:red">'. t('Offline'). '</span>';
      break;
  }
}

/**
* Preprocess the template for the display view
*/
function template_preprocess_sluser_terminals_list(&$variables) {
  $variables['terminals'] = $variables[0];
  foreach ($variables['terminals'] as $terminal) {
    drupal_add_js('$(document).ready(function(){$.get(\'./terminals/checkstatus/js\', {id:'. $terminal->id. '}, function(data) {$(\'#terminal-'. $terminal->id. '-status\').html(data);});})', 'inline');
  }
  // check if the user can admin the terminals
  global $user;
  $variables['allow_edit'] = user_access('administer sluser terminals', $user);
  $variables['pager'] = theme('pager');
}

/**
* Build the delete confirmation form.
*/
function sluser_terminal_delete_confirm(&$form_state, $terminal) {
  $machine_name = variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser');
  $form['terminal_id'] = array(
    '#type' => 'value',
    '#value' => $terminal->id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete terminal : %name ?', array('%name' => $terminal->name)),
    isset($_GET['destination']) ? $_GET['destination'] : $machine_name. '/terminals',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
* General submit handler for the delete confirmation form.
*/
function sluser_terminal_delete_confirm_submit($form, &$form_state) {
  $machine_name = variable_get('sluser_global_settings_terminology_sluser_machine_name',  'sluser');
  if ($form_state['values']['confirm']) {
    $deleted = sluser_terminal_delete($form_state['values']['terminal_id']);
    drupal_set_message(t('The terminal %name was deleted.', array('%name' => $deleted->name)));
  }
  $form_state['redirect'] = $machine_name. '/terminals';
}

/**
* Load a terminal list
*/
function sluser_terminal_load_all($page = 0, $limit = 10) {

  if (!isset($_GET['page'])) {
    $_GET['page'] = $page;
  }

  $terminals = array();
  $query = "SELECT * FROM {sluser_terminals}"
                . " ORDER BY sim_hostname ASC, region ASC";
  $query_result =  pager_query($query, $limit);
  while ($terminal = db_fetch_object($query_result)) {
    array_push($terminals, $terminal);
  }

  return $terminals;
}

/**
* Load the data for a single terminal
*/
function sluser_terminal_load($array) {
  // Dynamically compose a SQL query:
  $query = array();
  $params = array();

  if (is_numeric($array)) {
    $array = array('id' => $array);
  }
  elseif (!is_array($array)) {
    return FALSE;
  }

  foreach ($array as $key => $value) {
    if ($key == 'id') {
      $query[] = "$key = %d";
      $params[] = $value;
    }
    else {
      $query[]= "LOWER($key) = LOWER('%s')";
      $params[] = $value;
    }
  }
  $query = db_query('SELECT * FROM {sluser_terminals} u WHERE '. implode(' AND ', $query), $params);

  if (!$sluser_terminal = db_fetch_object($query)) {
    $sluser_terminal = FALSE;
  }

  return $sluser_terminal;
}

/**
* Store the termianl
*/
function sluser_terminal_save($values) {

  // convert values to an object
  $values = (object)$values;

  $response = array();
  // check if the terminal already exists
  $sluser_terminal = new stdclass;
  if ($values->name != '') {
    $sluser_terminal = sluser_terminal_load(array('uuid' =>$values->uuid));
  }

  if (!$sluser_terminal->id) {
    // insert new entry
    $response['op'] = drupal_write_record('sluser_terminals', $values);
  }
  else {
    // update entry
    $values->id = $sluser_terminal->id;
    $response['op'] = drupal_write_record('sluser_terminals', $values, 'id');
  }

  // define the result status
  $response['status'] = TRUE;
  switch ($response['op']) {
    case SAVED_NEW:
      $response['message'] = 'success;'. t('New terminal added.');
      watchdog('sluser', t('New terminal added by : %owner.'), $variables = array('%owner'=>$values->ownername), $severity = WATCHDOG_NOTICE, $link = NULL);
      break;
    case SAVED_UPDATED:
      $response['message'] = 'success;'. t('Terminal updated.');
      break;
    default:
      $response['status'] = FALSE;
      $response['message'] = 'error;'. t('Error saving terminal.').$response['op'];
      break;
  }
  $response['id'] = $values->id;
  return $response;
}

/**
* Delete the termsluser_terminal_deleteinal.
*/
function sluser_terminal_delete($terminal_id) {
  // get the terminal
  $sluser_terminal = sluser_terminal_load(array('id' =>$terminal_id));

  // remove terminal
  db_query('DELETE FROM {sluser_terminals} WHERE id = %d', $terminal_id);

  return $sluser_terminal;
}

/**
* Check password
*/
function sluser_terminal_check_password($password, $key) {
  if ($password == md5(variable_get('sluser_global_settings_terminals_password', "0000").':'.$key)) {
    return TRUE;
  }
  return FALSE;
}
